* tmux-rime 

** Separate C sources and Python scripts 
It's not quite easy to separate C sources from the Python scripts as below: 
#+begin_src 
├ ─ lib
│   ├── rime.c
│   ├── rime.h
│   └── rime.i
├── tmux_rime
│  ├── __init__.py
│  └── module.py
└── setup.py
#+end_src


Since executing =python setup.py build_py= will only copy all *.py files under the =packages= directory, which is =tmux_rime= in this example.
Check my note about [[file:tmux-rime_materials.org::*Distutils/Setuptools][Distutils/Setuptools]]  

The directory structure of the installed package is as follow:
#+begin_src 
.
├── __init__.py
├── module.py
├── _rime.cpython-38-x86_64-linux-gnu.so
└── _rime.py
#+end_src

The generated =rime.py= by =python setup.py build_ext=, or more precisely, by =swig -python -i rime.i=, does not appear in the dir.

*** Solution 
I add =-outdir tmux_rime= in =swig_opts= to output the generated swig interface file =rime.py= into the package directory =tmux-rime=.
#+begin_src python
rime = Extension('tmux_rime._rime', #name='_{}'.format(name),  # SWIG requires _ as a prefix for the module name
                 sources=["lib/rime.i", "lib/rime.c"],
                 libraries=['rime'],
                 extra_compile_args=["-std=c11"],
                 swig_opts=["-outdir", "tmux_rime"])
#+end_src



** Implement Rime Wrapper for Python
content 一定不是 None

clear_compostion 也會把 get_input_str 清掉


session id 取得方法
#+begin_src sh
tmux_rime_bind "$char"   "key -s '#{s/\\$//:session_id}' -k $char"
#+end_src

小心如果 key 送空白字元會被 striped
#+begin_src py
data = self.request.recv(1024).strip().decode('utf-8')
#+end_src

** Issues 
*** FIXED =send-input.sh= is not executed (f94fb2)
CLOSED: [2021-06-23 Wed 17:32]

- State "FIXED"      from "BUG"        [2021-06-23 Wed 17:32]
CLOSED: [2021-06-23 Wed 17:32]

If you execute =tmux_rime.sh= from =tmux='s =run-shell= , the =send-input.sh=, which is bound on keys in keytable =tmux_rime=, will not be executed      
#+begin_src sh
tmux bind-key "$TMUX_RIME_PREFIX" run-shell "$PLUGIN_DIR/scripts/tmux_rime.sh"
#+end_src

But if you execute it from command line, or =bash=, it will success 
#+begin_src sh
; "$PLUGIN_DIR/scripts/tmux_rime.sh"
#+end_src

**** Solution 
So my current solution is to create a temporary window tab, and run the script with =send-keys= in it.  
